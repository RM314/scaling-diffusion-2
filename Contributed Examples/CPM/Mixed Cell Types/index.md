---
MorpheusModelID: M2008

title: Mixed Cell Types

authors: [L. Edelstein-Keshet]
contributors: [Y. Xiao]

# Under review
hidden: false
private: false

# Reference details
#publication:
#  doi: ""
#  title: ""
#  journal: ""
#  volume:
#  issue:
#  page: ""
#  year:
#  original_model: true

tags:
- 2D
- Area
- Cell Shape
- Cellular Potts Model
- Contact
- CPM
- Interaction
- Interaction Cost
- Length
- LengthConstraint
- Medium
- Perimeter
- Shape
- Strength
- Surface
- SurfaceConstraint
- Volume
- VolumeConstraint

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
---
> Simulations of four types of cells with different shapes to demonstrate how the combined target volume, surface area, and medium contact interaction determine the shape of the cell

## Introduction

By setting the volume, surface, and length constraints, a CPM simulation can produce a variety of cell shapes.

## Description

In this example, there are four cell types with preferred volumes <span title="//CellTypes/CellType/VolumeConstraint/@target">$`V_i`$</span> (area in 2D), surfaces <span title="//CellTypes/CellType/SurfaceConstraint/@target">$`S_i`$</span> (perimeter in 2D) and lengths <span title="//CellTypes/CellType/LengthConstraint/@target">$`L_i`$</span> set to values:

- <span title="//CellTypes/CellType[name='ct1']/VolumeConstraint/@target">$`V_1`$</span>&nbsp;$`= 2100`$, <span title="//CellTypes/CellType[name='ct1']/LengthConstraint/@target">$`L_1`$</span>&nbsp;$`= 120`$ (red);
- <span title="//CellTypes/CellType[name='ct2']/VolumeConstraint/@target">$`V_2`$</span>&nbsp;$`= 1800`$, <span title="//CellTypes/CellType[name='ct2']/SurfaceConstraint/@target">$`S_2`$</span>&nbsp;$`= 250`$ (yellow);
- <span title="//CellTypes/CellType[name='ct3']/VolumeConstraint/@target">$`V_3`$</span>&nbsp;$`= 1800`$, <span title="//CellTypes/CellType[name='ct4']/SurfaceConstraint/@target">$`S_3`$</span>&nbsp;$`= 100`$ (green);
- <span title="//CellTypes/CellType[name='ct4']/VolumeConstraint/@target">$`V_4`$</span>&nbsp;$`= 2250`$, <span title="//CellTypes/CellType[name='ct4']/SurfaceConstraint/@target">$`S_4`$</span>&nbsp;$`= 400`$ (blue).

The <span title="//CellTypes/CellType/*Constraint/@strength">`strength`</span>s of all constraints is the same <span title="//CellTypes/CellType/*Constraint/@strength">$`\lambda`$</span>&nbsp;$`= 1`$.

## Results

The cells have all been assigned a low <span title="//CPM/Interaction/Contact[@type1='ct*'][@type2='medium']">`Interaction`</span> cost with the medium and a higher <span title="//CPM/Interaction/Contact[@type1='ct*'][@type2='ct*']">`Interaction`</span> cost with one another. Hence, we see that all cells avoid coming into contact.

![](MorpheusCellShapes.png "The cells prefer not to touch one another in this simulation, where the interaction energy with the medium is lower than that of the cells with one another.")

<figure>
  ![](M2008_mixed-cell-types_movie.mp4)
  <figcaption>
    Simulation video of {{< model_quick_access "media/model/m2008/MixedCellTypes.xml" >}}
  </figcaption>
</figure>