---
MorpheusModelID: M2001

title: CPM Parameter Plane

authors: [L. Edelstein-Keshet, J. Starruß]
contributors: [D. Jahn, Y. Xiao]

# Under review
#hidden: true
#private: true

# Reference details
publication:
  doi: "10.1186/s13628-015-0022-x"
  title: "The biophysical nature of cells: potential cell behaviours revealed by analytical and computational studies of cell surface mechanics"
  authors: [R. Magno, V. A. Grieneisen, A. F. Marée]
  journal: "BMC Biophys."
  volume: 8
  issue: 8
  page: "1-37"
  year: 2015
  #original_model: true

tags:
- Asphericity
- Cell-cell Contact
- CellDivision
- Cell-medium Contact
- Cellular Potts Model
- Circularity
- Contact Energy
- CPM
- Dimensionless Parameter
- Energetic Cost
- Parameter Plane
- Perimeter Cost
- Single Cell

categories:
- "L. Edelstein-Keshet: Mathematical Models in Cell Biology"
#- DOI:10.1186/s13628-015-0022-x
---
>Two-parameter plane showing all possible regimes of behavior for a single CPM cell

## Introduction

The idea is based on [figure 3](https://bmcbiophys.biomedcentral.com/articles/10.1186/s13628-015-0022-x/figures/3) in the [referenced paper](#reference).

## Description

But here, we use two **dimensionless parameters**:

- Dimensionless **perimeter cost**: <span title="//CellTypes/CellType/Property[@symbol='kappa']">$`\kappa = \frac{4 \lambda_\mathrm p}{\lambda_\mathrm a R_\mathrm a^2}`$</span> – on the horizontal axis,
- dimensionless **cell-medium contact energy cost**: <span title="//CellTypes/CellType/Property[@symbol='beta']">$`\beta = \frac{2J}{\pi \lambda_\mathrm a R_\mathrm a^3}`$</span> – on the vertical axis,
- **circularity**: <span title="//CellTypes/CellType/Property[@symbol='alpha']">$`\alpha = \frac{R_\mathrm p}{R_\mathrm a}`$</span> – vary manually.

## Results

![](CPMParameterPlane.png "A morpheus simulation of the shape of a single cell in the $`\kappa \beta`$ CPM parameter plane (<span title='//CellTypes/CellType/Property[@symbol=&#39;kappa&#39;]'>$`\kappa`$</span> is a scaled perimeter cost and <span title='//CellTypes/CellType/Property[@symbol=&#39;beta&#39;]'>$`\beta`$</span> is a scaled cell-medium contact energy, and <span title='//CellTypes/CellType/Property[@symbol=&#39;alpha&#39;]'>$`\alpha`$</span> is the asphericity of the cell). Produced with model file [`CPMParameterPlane_main.xml`](#model).")

![](TwoCellCPMParameterPlane.png "A morpheus simulation of a pair of cells showing the effect of scaled cell-cell and cell-medium energetic costs, <span title='//CellTypes/CellType/Property[@symbol=&#39;eta&#39;]'>$`\eta`$</span> and <span title='//CellTypes/CellType/Property[@symbol=&#39;beta&#39;]'>$`\beta`$</span>. Produced with model file [`TwoCellCPMParameterPlane.xml`](#downloads).")

<figure>
![](movie.mp4)
<figcaption>
Simulation of cells in the $`\kappa \beta`$ parameter plane with two plots, one colored by the <span title="//CellTypes/CellType/Property[@symbol='beta']">$`\beta`$</span> value and the other by the <span title="//CellTypes/CellType/Property[@symbol='kappa']">$`\kappa`$</span> value. Produced with model file {{< model_quick_access "media/model/m2001/CPMParameterPlane_main.xml" >}}.
</figcaption>
</figure>

{{% callout note %}}
**Suggestion:** Try the settings <span title="//CellTypes/CellType/Property[@symbol='alpha']">$`\alpha = 0.5, 1, 2`$</span>.
{{% /callout %}}

The results are now consistent with the theoretical analysis of [Magno *et al.*, 2015](#reference).
However, fine-tuning of the CPM settings was required to obtain the results.

![](plot_02000.png "Plots of the simulation result at time <span title='//Time/StopTime'>$t = 2000$</span>. One is colored by the <span title='//CellTypes/CellType/Property[@symbol=&#39;beta&#39;]'>$`\beta`$</span> value and the other by the <span title='//CellTypes/CellType/Property[@symbol=&#39;kappa&#39;]'>$`\kappa`$</span> value. Produced with model file [`CPMParameterPlane_main.xml`](#model).")
