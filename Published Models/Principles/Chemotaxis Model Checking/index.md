---
MorpheusModelID: M5496

authors: [O. Pârvu, D. Gilbert]
contributors: [L. Brusch]

title: "Chemotaxis for Model Checking"

# Reference details
publication:
  doi: "10.1186/s12918-014-0124-0"
  title: "Automatic validation of computational models using pseudo-3D spatio-temporal model checking."
  journal: "BMC Systems Biology"
  volume: 8
  page: 124
  year: 2014
  original_model: true
  
tags:
- 2D
- Adhesion
- Cellular Potts Model
- Chemotaxis
- CPM
- Model Checking
- Multicellular Model
- Spatial Model
- Stochastic Model

categories:
- DOI:10.1186/s12918-014-0124-0
---

## Introduction

Comparison of spatio-temporal model output to experimental measurements or between model variants all require the application of summary statistics. 
For multicellular models, statistical properties of the cell configuration at given times or across time are of interest and Morpheus provides some `Analysis/Tracker`s for that: `CellTracker`, `DisplacementTracker`, `ClusteringTracker` and `ContactLogger`.
To further capture the spatial shape of cell clusters and other properties, Pârvu and Gilbert have used the spatio-temporal markup language (STML), see [**Pârvu _et al._**](#reference) and their figure below. 

![](12918_2014_Article_124_Fig8_HTML.jpg "Simulation of chemotaxis and cell-cell adhesion (top row), segmentation of cell clusters (middle) and STML encoded cell clusters (bottom) as published in [Fig. 8](https://bmcsystbiol.biomedcentral.com/articles/10.1186/s12918-014-0124-0/figures/8) ([*CC BY 2.0*](https://creativecommons.org/licenses/by/2.0/): [**Pârvu _et al._**](#reference)). Time `t` counts output iterations which are written every 50 MCS.")

The STML-encoded model results were then evaluated in the model checker Mudi and compared to specified behaviour using probabilistic rules.
Mudi is an approximate probabilistic model checking platform that provides frequentist and Bayesian approaches for statistical hypothesis testing and model validation.
Note, the software Mudi was published on the webpage [http://mudi.modelchecking.org](http://mudi.modelchecking.org) but this is no longer online. 
The internet archive holds a [copy of the Mudi webpage from 2016](https://web.archive.org/web/20160619031929/http://mudi.modelchecking.org/).
The stochastic simulations had to be repeated many times (here by Mudi calling the Morpheus simulator without GUI on command line) to sample from the distribution of outcomes.


## Model Description

Space is discretized as a 2D square lattice with a lattice spacing (and spatial length unit) corresponding to 1 cell diameter.
A static scalar field `U(x,y)` provides the chemoattractant.
100 cells are initialized at random positions and the CPM dynamics accounts for random motility, chemotaxis and cell-cell adhesion.
The original model file was published as electronic supplementary material [12918_2014_124_MOESM9_ESM.zip](https://static-content.springer.com/esm/art%3A10.1186%2Fs12918-014-0124-0/MediaObjects/12918_2014_124_MOESM9_ESM.zip) and is also included in the download section below as {{< model_quick_access "media/model/m5496/model_published.xml" >}}.
This published model was run in the Morpheus version of 2014 and encoded in `<MorpheusModel version="1">`.
We've updated the model to the current `<MorpheusModel version="4">` such that it runs in the [latest version of Morpheus](/download/latest).
This updated {{< model_quick_access "media/model/m5496/model.xml" >}} is provided here.


## Results

Due to cell-cell adhesion, various cell clusters form during the chemotactic aggregation process until all cells have reached the single large cluster at the maximum of the static chemoattractant field. This process is visualized in the movie below (using the updated {{< model_quick_access "media/model/m9496/model.xml" >}}), where cells are shown yellow and the field `U(x,y)` in greyscale.

![](movie_reproduced.mp4)

Simulation snapshots qualitatively reproduce the results in the published Fig.8 above. Note, both are single samples of the stochastic process.

![](figure_reproduced.png "Cell configurations as simulated by the updated model and shown at time points that correspond to those in the published figure (here given in MCS below each panel instead of the output counter `t` above, e.g. 195 * 50 MCS = 9750 MCS).")
