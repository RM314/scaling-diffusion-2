{{% callout note %}}
This model also requires the separate file [`hexagon_200.tif`](#downloads).
{{% /callout %}}

![](model-graph.svg "Model Graph")