![](C2vsA2_modelgraph.svg "Model Graph of `C2vsA2_main.xml`")

{{% callout note %}}
For more information on the models **discussed here**,

- {{< model_quick_access "media/model/m7123/C2vsA2_main.xml" >}} (produced Fig. 5),
- {{< model_quick_access "media/model/m7123/qvsC2.xml" >}} (produced Fig. 6) and
- {{< model_quick_access "media/model/m7123/BiPlot.xml" >}} (produced SI Fig. S11),

see the [referenced paper](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf) as well as [supplemental information](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc1.pdf).

For information on models

- {{< model_quick_access "media/model/m7123/Biophys.xml" >}} (produced SI Fig. S12 by varying parameters `SurfaceConstraint` `strength` and the `VolumeConstraint` `strength`),
- {{< model_quick_access "media/model/m7123/NRAEcadCntr.xml" >}} (produced SI Fig. S8 by varying parameter `A2`) and
- {{< model_quick_access "media/model/m7123/NRARac1Cntr.xml" >}} (produced SI Fig. S8 by varying parameter $`\alpha`$)

which are here **attached but not discussed**, please refer to the [supplemental information](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc1.pdf) of the [referenced paper](https://ars.els-cdn.com/content/image/1-s2.0-S0006349522002843-mmc2.pdf).
{{% /callout %}}