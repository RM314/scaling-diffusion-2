<?xml version='1.0' encoding='UTF-8'?>
<MorpheusModel version="4">
    <Description>
        <Details>Naba Mukhtar, Eric N Cytrynbaum, and Leah Edelstein-Keshet (2022) A Multiscale computational model of YAP signaling in epithelial fingering behaviour

Produced SI Figure 7


We thank Lutz Brusch for providing a basic cell sheet simulation that we modified and adapted to this project </Details>
        <Title>NRA-Y-OE</Title>
    </Description>
    <Space>
        <Lattice class="square">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
            <Size symbol="size" value="400, 100, 0"/>
            <BoundaryConditions>
                <Condition type="constant" boundary="x"/>
                <Condition type="noflux" boundary="-x"/>
                <Condition type="periodic" boundary="y"/>
                <Condition type="periodic" boundary="-y"/>
            </BoundaryConditions>
        </Lattice>
        <SpaceSymbol symbol="space"/>
    </Space>
    <Time>
        <StartTime value="0"/>
        <StopTime symbol="stoptime" value="1500"/>
        <TimeSymbol symbol="time"/>
        <RandomSeed value="1"/>
    </Time>
    <Analysis>
        <Gnuplotter decorate="true" time-step="50">
            <Terminal name="png"/>
            <Plot>
                <Cells flooding="true" value="Y">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="lemonchiffon"/>
                            <Color value="0.05" color="light-blue"/>
                            <Color value="0.1" color="light-red"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot title="Rac1">
                <Cells min="0" max="3" flooding="true" value="R">
                    <ColorMap>
                        <Color value="0" color="blue"/>
                        <Color value="3" color="red"/>
                        <Color value="4" color="yellow"/>
                    </ColorMap>
                </Cells>
            </Plot>
            <Plot>
                <Cells min="0" max="2" flooding="true" value="E">
                    <Disabled>
                        <ColorMap>
                            <Color value="0" color="plum"/>
                            <Color value="4" color="blue"/>
                            <Color value="7" color="cyan"/>
                        </ColorMap>
                    </Disabled>
                </Cells>
            </Plot>
            <Plot>
                <Cells flooding="true" value="d.abs">
                    <!--    <Disabled>
        <ColorMap>
            <Color value="0" color="skyblue"/>
            <Color value="0.1" color="violet"/>
            <Color value="0.2" color="salmon"/>
        </ColorMap>
    </Disabled>
-->
                </Cells>
            </Plot>
            <!--    <Disabled>
        <Plot>
            <Cells value="Cr" flooding="true">
                <Disabled>
                    <ColorMap>
                        <Color value="0" color="red"/>
                        <Color value="0" color="red"/>
                    </ColorMap>
                </Disabled>
            </Cells>
        </Plot>
    </Disabled>
-->
        </Gnuplotter>
        <ModelGraph format="dot" include-tags="#untagged" reduced="false"/>
    </Analysis>
    <Global>
        <Constant symbol="ky" value="0.1" name="Basal rate of YAP activation"/>
        <Constant symbol="kye" value="1.8" name="E-cadherin-dependent rate of YAP deactivation"/>
        <Constant symbol="Dy" value="2" name="Inactivation rate of YAP"/>
        <Constant symbol="C" value="0.9" name="Initial activation rate of E-cadherin"/>
        <Constant symbol="ke" value="0.9" name="YAP-dependent rate of E-cadherin expression"/>
        <Constant symbol="K" value="1" name="Dissociation constant of YAP-WT1 transcriptional constant"/>
        <Constant symbol="De" value="1" name="Inactivation rate of E-cadherin"/>
        <Constant symbol="h" value="3" name="Hill coefficient for E-cadherin"/>
        <Constant symbol="kr" value="1" name="YAP-dependent rate of Rac1 expression"/>
        <Constant symbol="Kr" value="0.5" name="Michaelis-Menten-like constant for Rac1"/>
        <Constant symbol="Dr" value="0.5" name="Degradation rate of Rac1"/>
        <Constant symbol="n" value="6" name="Hill coefficient for Rac1"/>
        <Constant symbol="alphaR" value="1" name="Rac activation fraction"/>
        <Constant symbol="kyr" value="1.8" name="Rac1-dependent rate of YAP activation"/>
        <!--    <Disabled>
        <Constant value="2" symbol="A2" name="basal adhesion">
            <Annotation>E-cad blocking</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="A2" value="12" name="Max E-cadherin adhesion constant">
            <Annotation>Control</Annotation>
        </Constant>
        <Constant symbol="A3" value="0.85" name="E-cadherin half &quot;saturation&quot; "/>
        <Constant symbol="C1" value="0.4" name="basal migration"/>
        <Constant symbol="C2" value="4" name="Max Rac1 migration constant"/>
        <Constant symbol="C3" value="3" name="Rac1 half &quot;saturation&quot;"/>
        <Constant symbol="tlim" value="100" name="max time for initial leader cells to emerge"/>
        <Constant symbol="frac" value="0.2" name="fraction of neighbourhood Cr that the cell receives each time Cr spreads to it"/>
        <!--    <Disabled>
        <Constant value="0.5" symbol="Ytot" name="Total YAP (active (Y) + inactive)">
            <Annotation>KD</Annotation>
        </Constant>
    </Disabled>
-->
        <!--    <Disabled>
        <Constant symbol="Ytot" value="1.2" name="Total YAP (active (Y) + inactive)">
            <Annotation>Control</Annotation>
        </Constant>
    </Disabled>
-->
        <Constant symbol="Ytot" value="2" name="Total YAP (active (Y) + inactive)">
            <Annotation>OE</Annotation>
        </Constant>
        <Constant symbol="Rtot" value="5" name="Total Rac1 (active (R) + inactive)"/>
        <Constant symbol="shareprob" value="0.02" name="parameter that determines probability of Cr spread in each time step"/>
        <Variable symbol="E" value="0.0" name="E-cadherin"/>
        <Variable symbol="Cr" value="0.0" name="Basal Rac1 activation rate"/>
        <!--    <Disabled>
        <Variable value="0.0" symbol="x_edge2"/>
    </Disabled>
-->
        <!--    <Disabled>
        <Mapper name="leftmost edge cell" time-step="1.0">
            <Input value="(M>0)*cell.center.x*(cell.center.x>10) + (M==0)*size.x + (M>0)*size.x*(cell.center.x&lt;=10)"/>
            <Output mapping="minimum" symbol-ref="x_edge2"/>
        </Mapper>
    </Disabled>
-->
    </Global>
    <CellTypes>
        <CellType class="medium" name="medium"/>
        <CellType class="biological" name="dividingcell">
            <VolumeConstraint strength="1" target="50"/>
            <ConnectivityConstraint/>
            <SurfaceConstraint strength="1" mode="aspherity" target="1"/>
            <CellDivision division-plane="major">
                <Condition>(rand_uni(0,1)&lt;0.006*(exp(-time/300)+0.2)) * (cell.center.x&lt;40)</Condition>
                <Triggers/>
                <Annotation>Only cells in the first 40 pixels can divide, regardless of domain size. Before, this was size.x*0.1</Annotation>
            </CellDivision>
            <CellDeath name="delete cells near right domain edge">
                <Condition>(cell.center.x>0.99*size.x)</Condition>
            </CellDeath>
            <DirectedMotion strength="C1+C2*R/(C3+R)" name="cell migration" direction="1, 0.0, 0.0"/>
            <Property symbol="Y" value="0" name="YAP"/>
            <Property symbol="E" value="10" name="E-cadherin"/>
            <Property symbol="R" value="0" name="Rac1"/>
            <Property symbol="Cr" value="0.001"/>
            <Property symbol="dist" value="0.0" name="distance from right domain edge"/>
            <Property symbol="avspeed" value="0.0" name="Sum of instantaneous speeds each time step (times 100)"/>
            <Property symbol="truavspeed" value="0.0" name="Average Speed * 100"/>
            <Property symbol="avspeed2" value="0.0" name="Sum of instantaneous speeds over final 200 time steps (times 100)"/>
            <Property symbol="truavspeed2" value="0.0" name="average speed over final 200 time steps (times 100)"/>
            <Property symbol="M" value="0.0" name="Contact with medium"/>
            <Property symbol="neigh" value="0.0" name="number of neighbour cells"/>
            <Property symbol="av" value="0.0" name="average neighbourhood Cr"/>
            <PropertyVector symbol="d" value="0.0, 0.0, 0.0" name="speed"/>
            <NeighborhoodReporter>
                <Input scaling="length" value="cell.type == celltype.medium.id"/>
                <Output symbol-ref="M" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="cell.type == celltype.dividingcell.id"/>
                <Output symbol-ref="neigh" mapping="sum"/>
            </NeighborhoodReporter>
            <NeighborhoodReporter>
                <Input scaling="cell" value="Cr"/>
                <Output symbol-ref="av" mapping="average"/>
            </NeighborhoodReporter>
            <MotilityReporter time-step="50">
                <Velocity symbol-ref="d"/>
            </MotilityReporter>
            <System solver="Dormand-Prince [adaptive, O(5)]">
                <DiffEqn symbol-ref="Y" name="Equation for YAP">
                    <Expression>(ky+kyr*R)*(Ytot-Y) - (kye*Y*E + Dy*Y)</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="E" name="Equation for E-cadherin">
                    <Expression>C - ke*Y^h/(K^h+Y^h) - De*E</Expression>
                </DiffEqn>
                <DiffEqn symbol-ref="R" name="Equation for Rac1">
                    <Expression>alphaR*(Cr + kr*Y^n/(Kr^n+Y^n))*(Rtot-R) - Dr*R</Expression>
                </DiffEqn>
                <Rule symbol-ref="Cr" name="Equation for Cr spread; at each time step before tlim, cells in contact with the medium have a small chance of being endowed with high Cr; at each time step, each cell has a small chance of having its Cr be augmented by the average neighbourhood Cr (up to a max value); cells at the left edge of the domain retain low Cr">
                    <Expression>Cr+(0.1*(M>8)*(rand_uni(0,1)&lt;0.008)*(time&lt;tlim)+frac*av*(rand_uni(0,1)&lt;shareprob))*(Cr&lt;0.1)*(cell.center.x>20)</Expression>
                </Rule>
                <Rule symbol-ref="dist" name="distance from right domain edge">
                    <Expression>size.x - cell.center.x</Expression>
                </Rule>
                <Rule symbol-ref="avspeed" name="Sum of instantaneous speeds each time step (times 100)">
                    <Expression>avspeed+d.abs*100</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed" name="average speed over the simulation (times 100)">
                    <Expression>avspeed/time</Expression>
                </Rule>
                <Rule symbol-ref="avspeed2" name="Sum of instantaneous speeds over final 100 time steps (times 100)">
                    <Expression>avspeed2 + d.abs*100*(time > stoptime-201)</Expression>
                </Rule>
                <Rule symbol-ref="truavspeed2" name="average speed over 100 time steps (times 100)">
                    <Expression>avspeed2/200</Expression>
                </Rule>
            </System>
        </CellType>
    </CellTypes>
    <CellPopulations>
        <Population size="1" type="dividingcell" name="Initialize cell sheet at left edge of the domain">
            <!--    <Disabled>
        <InitRectangle mode="regular" number-of-cells="70">
            <Dimensions origin="size.x/100, 0, 0" size="size.x/100, size.y, size.z"/>
        </InitRectangle>
    </Disabled>
-->
            <InitRectangle number-of-cells="70" mode="regular">
                <Dimensions size="4.0, size.y, 0.0" origin="0.0, 0.0, 0.0"/>
            </InitRectangle>
        </Population>
    </CellPopulations>
    <CPM>
        <Interaction>
            <Contact type2="dividingcell" type1="dividingcell" value="30">
                <AddonAdhesion strength="5" adhesive="A2*E/(A3+E)" name="Adhesive strength"/>
            </Contact>
            <Contact type2="medium" type1="dividingcell" value="12"/>
        </Interaction>
        <ShapeSurface scaling="norm">
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </ShapeSurface>
        <MonteCarloSampler stepper="edgelist">
            <MCSDuration value="1"/>
            <MetropolisKinetics temperature="1"/>
            <Neighborhood>
                <Order>2</Order>
            </Neighborhood>
        </MonteCarloSampler>
    </CPM>
</MorpheusModel>
